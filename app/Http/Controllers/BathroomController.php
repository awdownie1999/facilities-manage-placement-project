<?php

namespace App\Http\Controllers;

use App\Bathroom;
use App\Site;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class BathroomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        Log::info('siteID' . $request->get('site_id'));
        $bathrooms = Bathroom::where('site_id', $request->get('site_id'))->get();
        Log::info('bathrooms' . $bathrooms);
        return view('Bathroom.index', compact('bathrooms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bathrooms = Bathroom::all();
        $sites = Site::all();
        return view('Bathroom.addBathroom', compact('bathrooms', 'sites'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate
        $request->validate([
            'sinks' => 'integer|required',
            'toilets' => 'integer|required',
        ]);

        //store
        $bathroom = Bathroom::firstOrNew([
            'sinks' => $request->get('sinks'),
            'toilets' => $request->get('toilets'),
            'site_id' => $request->get('site'),
        ]);

        if($request->hasFile('file1') && $request->file('file1')->isValid()){
            $bathroom->addMediaFromRequest('file1')->toMediaCollection('bathroom_images');
        }

        if($request->hasFile('file2') && $request->file('file2')->isValid()){
            $bathroom->addMediaFromRequest('file2')->toMediaCollection('bathroom_images');
        }

        if($request->hasFile('file3') && $request->file('file3')->isValid()){
            $bathroom->addMediaFromRequest('file3')->toMediaCollection('bathroom_images');
        }
        $bathroom->save();
        return redirect('/sites');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bathroom  $bathroom
     * @return \Illuminate\Http\Response
     */
    public function show(Bathroom $bathroom)
    {
        return view('Bathroom.bathroomDetails', compact('bathroom'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bathroom  $bathroom
     * @return \Illuminate\Http\Response
     */
    public function edit(Bathroom $bathroom)
    {
        return view('Bathroom.editBathroom', compact('bathroom'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bathroom  $bathroom
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bathroom $bathroom)
    {
        //validate
        $request->validate([
            'sinks' => 'required|integer',
            'toilets' => 'required|integer'
        ]);
        //update
        $bathroom->sinks = $request->get('sinks');
        $bathroom->toilets = $request->get('toilets');
        //image
        $bathroom->save();
        if($request->hasFile('file1') && $request->file('file1')->isValid()){
            $bathroom->addMediaFromRequest('file1')->toMediaCollection('bathroom_images');
            Log::info('Data');
        }
        $bathroom->save();
        return redirect('/sites');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bathroom  $bathroom
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bathroom $bathroom)
    {
        $bathroom->delete();
        return redirect('/sites');
    }
}
