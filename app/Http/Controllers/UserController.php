<?php

namespace App\Http\Controllers;

use App\User;
use App\Job;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Log;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->get('filter');
        $assignedUsers = array();
        $unassignedUsers = array();
        $jobs = Job::all();

        foreach($jobs as $job){
            if($job->user_id !=null){
                array_push($assignedUsers, $job->user); 
            }
        }
        $users = User::role('staff')->get();
        foreach($users as $user){
            if(!in_array($user, $assignedUsers)){
                array_push($unassignedUsers, $user);
            }
        }
        //$users = User::whereHas("roles", function(Builder $q) {$q->where("name", "staff"); })->get();

        if($filter == null || $filter == 'all'){
            $users = User::paginate(5);
        }else if($filter == 'assigned'){
            $users = $this->paginate($assignedUsers);
        }else if($filter == 'unassigned'){
            $users = $this->paginate($unassignedUsers);
        }
        if(count($users) > 0){
            $filter = $request->get('filter');
        }else{
            $filter = "empty";
        }
        return view('User.userIndex', compact('users', 'filter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        return view('User.createUser', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|max:255',
            'password' => 'required|string|min:8',
            'address' => 'required|string|min:5',
            'contact_num' => 'required|numeric|min:11',
        ]);
        $user = User::firstOrNew([
            'email' => $request['email'],
        ]);
        Log::info('Address Check : ' . $request->get('address'));

        $user->fill([
            'name' => $request->name,
            'address' => $request->address,
            'contact_num' => $request->contact_num,
            'password' => Hash::make($request->password),
            ]);
            $user->assignRole('staff'); 
            $user->save();

        return redirect('/users')->with('User added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Site  $site
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Site  $site
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('User.editUser', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Site  $site
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|max:255',
            'address' => 'required|string|min:5',
            'contact_num' => 'required|numeric|min:11',
        ]);

        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->address = $request->get('address');
        $user->contact_num = $request->get('contact_num');
        $user->save();

        $user->assignRole('staff'); 
        return redirect('/users')->with('updated user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Site  $site
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return redirect('/users')->with('User deleted!');
    }

    public function paginate($items, $perPage = 5, $page = null, $options = [])
    {
    $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
    $items = $items instanceof Collection ? $items : Collection::make($items);
    return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
}
