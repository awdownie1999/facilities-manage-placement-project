<?php

namespace App\Http\Controllers;

use App\Bathroom;
use App\Job;
use App\Organisation;
use App\Site;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->get('filter');
        if($filter == null|| $filter == 'all'){
            $jobs = Job::paginate(5);
        }elseif($filter == 'completed'){
            $completedJobs = Job::where('complete', true)->get();
            $jobs = $this->paginate($completedJobs);
        }elseif($filter == 'in-complete'){
            $incompleteJobs = Job::where('complete', false)->get();
            $jobs = $this->paginate($incompleteJobs);
        }
        if(count($jobs) > 0 ){
            $filter = $filter;
        }else{
            $filter = 'empty';
        }
        return view('Jobs.jobIndex', compact('jobs', 'filter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jobs = Job::all();
        $users = User::role('staff')->get();
        $organisations = Organisation::all();
        $sites = Site::all();
        $bathrooms = Bathroom::all();
        return view('Jobs.createJob', compact('jobs', 'organisations', 'users', 'sites', 'bathrooms'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //store
        $job = Job::firstOrNew([
            'bathroom_id' => $request->get('bathroom'),
            'user_id' => $request->get('user'),
        ]);

        $job->save();
        return redirect('/jobs');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function show(Job $job)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function edit(Job $job)
    {
        return view('Jobs.updateJob', compact('job'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Job $job)
    {
        //update
        if($request->get('complete') == 'on'){
            $job->complete = true;
        }else{
            $job->complete = false;
        }
        $job->save();
        return redirect('/jobs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function destroy(Job $job)
    {
        $job->delete();
        return redirect('/jobs');
    }

    public function paginate($items, $perPage = 5, $page = null, $options = [])
    {
    $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
    $items = $items instanceof Collection ? $items : Collection::make($items);
    return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
}
