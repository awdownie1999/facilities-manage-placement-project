<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Account.account');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user =  User::firstOrNew([
            'name' => Auth::user()->name,
            'email' => Auth::user()->email,
            'password' => Auth::user()->password,
            'address' => Auth::user()->address,
            'contact_num' => Auth::user()->contact_num,
        ]);

        if($request->hasFile('avatar') && $request->file('avatar')->isValid()){
            $user->addMediaFromRequest('avatar')->toMediaCollection('avatars');
        }

        return redirect('/account');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bathroom  $bathroom
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bathroom  $bathroom
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $user = Auth::user();
        return view('Account.editAccount', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bathroom  $bathroom
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        //Validation
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|max:255',
            'address' => 'required|string|min:10',
            'contact_num' => 'required|numeric|min:11',
        ]);

        //Update
        $user = User::find($id);
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->address = $request->get('address');
        $user->contact_num = $request->get('contact_num');
        $user->save();
        return redirect('/account');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bathroom  $bathroom
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $userMedia = User::find($id)->getMedia('avatars')->first();
        $userMedia->delete();
        return redirect('/account')->with('deleted');
    }
}
