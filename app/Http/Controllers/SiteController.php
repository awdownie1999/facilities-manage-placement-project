<?php

namespace App\Http\Controllers;

use App\Organisation;
use App\Site;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;


class SiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sites = Site::Paginate(5);
        return view('Site.siteIndex', compact('sites'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sites = Site::all();
        $organisations = Organisation::all();
        return view('Site.createSite', compact('sites', 'organisations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Log::info('message');
        //Validate
        $request->validate([
            'location' => 'required|string'
        ]);

        //Store
        $site = Site::firstOrNew([
            'location' => $request->get('location'),
            'organisation_id' => $request->get('organisation'),
            ]);
        $site->save();
        Log::info('site check' . $site);
        return redirect('/sites')->with('Site created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Site  $site
     * @return \Illuminate\Http\Response
     */
    public function show(Site $site)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Site  $site
     * @return \Illuminate\Http\Response
     */
    public function edit(Site $site)
    {
        return view('Site.editSite', compact('site'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Site  $site
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Site $site)
    {
        //Validate
        $request->validate([
            'location' => 'required|string'
        ]);

        //Update
        $site->location = $request->get('location');
        $site->save();
        return redirect('/sites');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Site  $site
     * @return \Illuminate\Http\Response
     */
    public function destroy(Site $site)
    {
        $site->delete();
        return redirect('/sites');
    }
}
