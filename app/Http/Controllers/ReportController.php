<?php

namespace App\Http\Controllers;

use App\Bathroom;
use App\Report;
use App\Site;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reports = Report::all();
        return view('Report.reportIndex', compact('reports'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $reports = Report::all();
        $bathrooms = Bathroom::all();
        $sites = Site::all();
        return view('Report.createReport', compact('reports', 'sites', 'bathrooms'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'description' => 'required|string',
        ]);


        $report = Report::firstOrNew([
            'description' => $request->get('description'),
            'bathroom_id' => $request->get('bathroom_id')
        ]);
        $report->save();

        if($request->hasFile('file1') && $request->file('file1')->isValid()){
            $report->addMediaFromRequest('file1')->toMediaCollection('report_images');
        }

        if($request->hasFile('file2') && $request->file('file2')->isValid()){
            $report->addMediaFromRequest('file2')->toMediaCollection('report_images');
        }

        if($request->hasFile('file3') && $request->file('file3')->isValid()){
            $report->addMediaFromRequest('file3')->toMediaCollection('report_images');
        }
        $report->save();

        return redirect('/records');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function show(Report $report)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function edit(Report $report)
    {
        return view('Report.editReport', compact('report'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Report $report)
    {
        $request->validate([
            'description' => 'required|string',
        ]);

        $report->description = $request->get('description');
        $report->save();

        if($request->hasFile('file1') && $request->file('file1')->isValid()){
            $report->addMediaFromRequest('file1')->toMediaCollection('report_images');
        }
        return redirect('/reports')->with('updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function destroy(Report $report)
    {
        $report->delete();
        return redirect('/reports');
    }
}
