<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Organisation;
use App\Report;
use App\Job;
use App\Bathroom;


class DashController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reports = Report::all();
        $totalReports = count($reports);
        $organisations = Organisation::all();
        $totalOrg = count($organisations);
        $bathrooms = Bathroom::all();
        $totalBathrooms = count($bathrooms);
        $jobs = Job::where('complete', false)->get();
        $jobsInProgress = count($jobs);

        return view('Dash.dash', compact('totalReports', 'totalOrg', 'totalBathrooms', 'jobsInProgress'));


    }

   
}
