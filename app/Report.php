<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Report extends Model implements HasMedia
{
    use HasMediaTrait;
    
    public function bathroom() {
        return $this->belongsTo('App\Bathroom');
    }

    protected $fillable = [
        'description', 'bathroom_id'
    ];

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('small')
            ->width(150)
            ->height(150);
    }
}
