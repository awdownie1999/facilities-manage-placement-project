<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    public function bathroom() {
        return $this->hasMany('App\Bathroom');
    }

    public function organisation() {
        return $this->belongsTo('App\Organisation');
    }

    protected $fillable = [
        'location', 'organisation_id', 'password',
    ];
}
