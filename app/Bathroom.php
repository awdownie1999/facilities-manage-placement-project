<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Bathroom extends Model implements HasMedia
{
    use HasMediaTrait;
    
    public function job() {
        return $this->hasMany('App\Job');
    }

    public function report() {
        return $this->hasMany('App\Report');
    }

    public function site() {
        return $this->belongsTo('App\Site');
    }

    protected $fillable = [
        'sinks', 'toilets', 'site_id',
    ];

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('small')
            ->width(150)
            ->height(150);
    }
}
