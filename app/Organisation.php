<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organisation extends Model
{

    public function site() {
        return $this->hasMany('App\Site');
    }

    protected $fillable = [
        'name', 'contact_number'
    ];
}
