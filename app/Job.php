<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    public function bathroom() {
        return $this->belongsTo('App\Bathroom');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

    protected $fillable = [
        'complete', 'user_id', 'bathroom_id',
    ];
}
