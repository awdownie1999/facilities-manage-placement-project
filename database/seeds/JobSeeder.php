<?php

use Illuminate\Database\Seeder;
use App\Job;

class JobSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $job1 = Job::firstOrNew([

            'complete' => true,
            'bathroom_id' => 1,
            'user_id' => 2,
        ]);
        $job1->save();

        $job2 = Job::firstOrNew([

            'complete' => false,
            'bathroom_id' => 1,
            'user_id' => 3,
        ]);
        $job2->save();
    }
}
