<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //Set Roles
        $manager = Role::create(['name' => 'Manager']);
        $managerPermission = Permission::create(['name' => 'all']);
        $staff = Role::create(['name'=> 'staff']);
        $staffPermission = Permission::create(['name' => 'min']);
        $manager->save();
        $managerPermission->save();
        $staff->save();
        $staffPermission->save();



        //Manager User
        $owner = User::FirstOrNew([
            'name' => 'manager',
            'email' => 'admin@example.com',
            'password' => Hash::make('password'),
            'address' => '12 Manager Way',
            'contact_num' => '12345678901',
        ]);
        $getManagerRole = Role::findByID(1);
        $owner->givePermissionTo($managerPermission);
        $owner->assignRole($getManagerRole);
        $owner->save(); 



        //Staff Members

        $staff1 = User::FirstOrNew([
            'name' => 'staff1',
            'email' => 'staff@example.com',
            'password' => Hash::make('password'),
            'address' => '12 Staff Way',
            'contact_num' => '12345678901',
        ]);
        
        $getStaffRole = Role::findById(2);
        $staff1->givePermissionTo($staffPermission);
        $staff1->assignRole($getStaffRole);
        $staff1->save();


        $staff2 = User::FirstOrNew([
            'name' => 'staff2',
            'email' => 'staff2@example.com',
            'password' => Hash::make('password'),
            'address' => '12 Staff Way',
            'contact_num' => '12345678901',
        ]);
        
        $getStaffRole = Role::findById(2);
        $staff2->givePermissionTo($staffPermission);
        $staff2->assignRole($getStaffRole);
        $staff2->save();
    }
}
