<?php

use Illuminate\Database\Seeder;
use App\Bathroom;

class BathroomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bathroom1 = Bathroom::firstOrNew([
            'sinks' => 5,
            'toilets' => 11,
            'site_id' => 1,
        ]);
        $bathroom1->save();

        $bathroom2 = Bathroom::firstOrNew([
            'sinks' => 1,
            'toilets' => 2,
            'site_id' => 1,
        ]);
        $bathroom2->save();
    }
}
