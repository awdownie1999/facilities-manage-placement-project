<?php

use Illuminate\Database\Seeder;
use App\Organisation;

class OrganisationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $wp = Organisation::FirstOrNew([
            'name' => 'Windsor Park',
            'contact_number' => '12345678901',
        ]);
        $wp->save();
    }
}
