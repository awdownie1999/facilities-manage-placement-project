<?php

use Illuminate\Database\Seeder;
use App\Report;

class ReportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $report1 = Report::firstOrNew([

            'description' => 'Bathrooms kept up to standard',
            'bathroom_id' => 2,
        ]);
        $report1->save();
    }
}
