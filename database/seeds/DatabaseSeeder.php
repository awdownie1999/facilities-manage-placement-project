<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            OrganisationSeeder::class,
            UserSeeder::class,
            SiteSeeder::class,
            BathroomSeeder::class,
            ReportSeeder::class,
            JobSeeder::class,

         ]);
    }
}
