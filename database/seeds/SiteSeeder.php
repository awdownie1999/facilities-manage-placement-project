<?php

use Illuminate\Database\Seeder;
use App\Site;

class SiteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mcdSite1 = Site::firstOrNew([
            'location' =>  '23-25 Greenway Blocks',
            'organisation_id' => 1,
        ]);
        $mcdSite1->save();

        $mcdSite2 = Site::firstOrNew([
            'location' =>  '16 New Road',
            'organisation_id' => 1,
        ]);
        $mcdSite2->save();
    }
}
