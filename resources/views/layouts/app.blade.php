<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title >{{ config('app.name', 'JAS') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="{{asset('js/filter.js')}}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/dash') }}">
                    {{ config('app.name', 'JAS') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <li class="dropdown"><a class="dropdown-toggle"  data-toggle="dropdown" href="#"> <font size="4" color = "#1C713E">Sites</font>  </a>
                            <ul class="dropdown-menu">
                                <li><a href="/sites"><font size="4" color = "#1C713E">&nbsp;&nbsp;&nbsp;Index</font></a></li>
                                <li><a href="/sites/create"><font size="4" color = "#1C713E">&nbsp;&nbsp;&nbsp;Create</font></a></li>
                            </ul>
                        </li>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        
                            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><font size="4" color = "#1C713E">Jobs</font></a>
                                <ul class="dropdown-menu">
                                    <li><a href="jobs"><font size="4" color = "#1C713E">&nbsp;&nbsp;&nbsp;Index</font></a></li>
                                <li><a href="jobs/create"><font size="4" color = "#1C713E">&nbsp;&nbsp;&nbsp;Create</font></a></li>
                                </ul>
                            </li>

                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        
                            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><font size="4" color = "#1C713E">Reports</font></a>
                                <ul class="dropdown-menu">
                                    <li><a href="reports"><font size="4" color = "#1C713E">&nbsp;&nbsp;&nbsp;Index</font></a></li>
                                <li><a href="reports/create"><font size="4" color = "#1C713E">&nbsp;&nbsp;&nbsp;Create</font></a></li>
                                </ul>
                            </li>
                        
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        @role('Manager')
                        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><font size="4" color = "#1C713E">Users</font></a>
                            <ul class="dropdown-menu">
                                <li><a href="/users"><font size="4" color = "#1C713E">&nbsp;&nbsp;&nbsp;Index</font></a></li>
                                <li><a href="/users/create"><font size="4" color = "#1C713E">&nbsp;&nbsp;&nbsp;Create</font></a></li>
                            </ul>
                        </li>
                        @endrole
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        @role('Manager')
                            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><font size="4" color = "#1C713E">Organisations</font></a>
                                <ul class="dropdown-menu">
                                    <li><a href="/organisations"><font size="4" color = "#1C713E">&nbsp;&nbsp;&nbsp;Index</font></a></li>
                                    <li><a href="/organisations/create"><font size="4" color = "#1C713E">&nbsp;&nbsp;&nbsp;Create</font></a></li>
                                </ul>
                            </li>
                        @endrole
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto" >
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    @if(Auth::user()->getMedia('avatars')->first() != null)
                                        <img src="{{ Auth::user()->getFirstMediaUrl('avatars', 'thumb') }}">
                                    @endif
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item"  href="/account">{{ __('Account') }}</a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
