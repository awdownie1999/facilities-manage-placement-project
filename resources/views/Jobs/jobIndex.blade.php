@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Jobs</h1>
        <div>
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
        @endif
        @role('Manager')
            <div><a style="margin:19px;" href="{{route('jobs.create')}}" class="btn btn-primary">New Job</a>
        @endrole   
            <a style="margin:19px;" href="{{route('jobs.index', ['filter'=> 'all'])}}" class="btn btn-primary">All</a>
            <a style="margin:19px;" href="{{route('jobs.index', ['filter'=> 'in-complete'])}}" class="btn btn-primary">In-Complete Jobs</a>
            <a style="margin:19px;" href="{{route('jobs.index', ['filter'=> 'completed'])}}" class="btn btn-primary">Completed Jobs</a></div>
        @if($filter != 'empty')
            <table><thead><tr>
                <th>Complete</th>
                <th>&nbsp;&nbsp;Bathroom</th>
                <th>&nbsp;&nbsp;Organisation</th>
                <th>&nbsp;&nbsp;Site</th>
                <th>&nbsp;&nbsp;User</th>
            </tr></thead><tbody>
                @foreach($jobs as $job)
                <tr>
                    @if($job->complete == true)
                        <td>Yes</td>
                    @else
                        <td>No</td>
                    @endif
                    <td>&nbsp;&nbsp;{{$job->bathroom->id}}</td>
                    <td>&nbsp;&nbsp;{{$job->bathroom->site->location}}</td>
                    <td>&nbsp;&nbsp;{{$job->bathroom->site->organisation->name}}</td>
                    <td>&nbsp;&nbsp;{{$job->user->name}}</td>
                    @if($job->user_id == Auth::user()->id)
                        <td>&nbsp;&nbsp;<a href="{{route('jobs.edit', $job->id)}}" class="btn btn-primary">Update</a></td>
                    @endif
                    @role('Manager')
                        <td><form action="{{route('jobs.destroy', $job->id)}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit"> Delete</button></form></td>
                    @endrole   
                </tr>
                @endforeach</tbody>
            </table>
        @else
        <h2>No Jobs matching criteria</h2>
        @endif

        <div class="row">
            <div class="col-12 d-flex justify-content-center pt-4">
                {{$jobs->links()}}
            </div>
        </div>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection