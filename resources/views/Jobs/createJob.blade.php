@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Create Job</h1>
        <div>
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
        @endif
            <form>
                @csrf
                <div>
                    <label for="user">User</label>
                    <select name="user">
                        @foreach($users as $user)
                            <option value="{{$user->id}}">{{$user->name}}</option>
                        @endforeach
                    </select>
                </div>
                <br/>
                <div>
                    <label for="organisation">Organisation</label>
                    <select name="organisation">
                        @foreach($organisations as $organisation)
                            <option value="{{$organisation->id}}">{{$organisation->name}}</option>
                        @endforeach
                    </select>
                </div>
                <br/>
                <div>
                    <label for="site">Site</label>
                    <select name="site">
                        @foreach($sites as $site)
                            <option value="{{$site->id}}">{{$site->location}}</option>
                        @endforeach
                    </select>
                </div>
                <br/>
               {{-- Have this populate  based off Site  --}}
                <div>
                    <label for="bathroom">Bathroom</label>
                    <select name="bathroom">
                        @foreach($bathrooms as $bathroom)
                            <option value="{{$bathroom->id}}">{{$bathroom->id}}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-secondary">Create</button>
            </form>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection