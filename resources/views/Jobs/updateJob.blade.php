@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Update Job</h1>
        <div>
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
        @endif
            <form method="POST" action="{{route('jobs.update', $job->id)}}">
                @method('PATCH')
                @csrf
                <br/><br/><br/>
                <div>
                    <label style="font-size:40px;" for="complete">Complete?</label>
                    &nbsp;&nbsp;&nbsp;<input type="checkbox" name="complete" style="transform: scale(3.0);"/>
                </div><br/><br/><br/>
                <button type="submit" class="btn btn-secondary">Update</button>
            </form>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection