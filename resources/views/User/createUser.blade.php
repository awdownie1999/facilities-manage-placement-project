@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Create User</h1>
        <div>
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
        @endif
        @role('Manager')
        <form method="post" action="{{ route('users.store') }}">
            @csrf
            <div class="form-group">
                <label for="name">name:</label>
                <input type="text" class="form-control" name="name" />
            </div>
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" class="form-control" name="email"/>
            </div>
            <div class="form-group">
                <label for=password>Password</label>
                <input type="password" class="form-control" name="password"/>
            <div>
                <label for="contactNumber">Contact Number</label>
                <input type="string" class="form-control" name="contact_num"/>
            </div>
            <div>
                <label for="address">Address</label>
                <input type="string" class="form-control" name="address"/>
            </div>
            <br/><br/>
            <button type="submit" class="btn btn-secondary">Add User</button>
        </form>
    @endrole
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection