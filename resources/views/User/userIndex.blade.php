@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Users</h1>
        <div>
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
        @endif
        <div id="msg"> </div>
        <div id="newdiv"><h6>data</h6></div>
        @role('Manager')
        <div><a style="margin: 19px;" href ="{{ route('users.create') }}" class="btn btn-primary">New User</a>
            <a style="margin: 19px;" href ="{{ route('users.index', ['filter'=> 'all']) }}" class="btn btn-primary">All</a>
            <a style="margin: 19px;" href ="{{ route('users.index', ['filter'=> 'assigned']) }}" class="btn btn-primary">Assigned</a>
            <a style="margin: 19px;" href ="{{ route('users.index', ['filter'=> 'unassigned']) }}" class="btn btn-primary">Unassigned</a></div>
            <input type="text" id="filterInput" placeholder="search">
            @if($filter != 'empty')
          
                <table>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>Contact Number</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->address }}</td>
                                <td>{{ $user->contact_num }}</td>
                                <td><a href="{{route('users.edit', $user->id)}}" class="btn btn-primary">Edit</a></td>
                                <td><form action="{{route('users.destroy', $user->id)}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit"> Delete</button></form></td>   
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <h1>No results matching Criteria</h1>
            @endif
        @endrole

        <div class="row">
            <div class="col-12 d-flex justify-content-center pt-4">
                {{$users->links()}}
            </div>
        </div>

        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection