@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Edit User Details</h1>
        <div>
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
        @endif
        @role('Manager')
        <form method="post" action="{{ route('users.update', $user->id)}}">
            @method('PATCH')
            @csrf
            <div class="form-group">
                <label for="name">name:</label>
                <input type="text" class="form-control" name="name" value="{{ $user->name }}">
            </div>
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" class="form-control" name="email" value="{{$user->email}}">
            </div>
            <div class="form-group">
                <label for="address">address:</label>
                <input type="text" class="form-control" name="address" value="{{ $user->address }}">
            </div>
            <div class="form-group">
                <label for="contact_num">Contact Number:</label>
                <input type="text" class="form-control" name="contact_num" value="{{ $user->contact_num }}">
            </div>
            <button type="submit" class="btn btn-secondary">Add User</button>
        </form>
        @endrole

        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection