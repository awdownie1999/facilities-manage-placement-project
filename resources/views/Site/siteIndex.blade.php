@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Sites</h1>
        <div>
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
        @endif
        @role('Manager')
            <a href="{{route('sites.create')}}" class="btn btn-primary">New Site</a>
            <a href="{{route('bathrooms.create')}}" class="btn btn-primary">New Bathroom</a>
        @endrole
        <table>
            <thead>
                <tr>
                    <th>Location</th>
                    <th>Organisation</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($sites as $site )
                    <tr>
                        <td>{{$site->location}}</td>
                        <td>{{$site->organisation->name}}</td>
                        <td><a href="{{route('bathrooms.index', ['site_id'=>$site->id])}}" class="btn btn-primary">View Bathrooms</a></td>
                        @role('Manager')
                            <td><a href="{{route('sites.edit', $site->id)}}" class="btn btn-primary">Edit</a></td>
                            <td><form action="{{route('sites.destroy', $site->id)}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" type="submit"> Delete</button></form></td>
                        @endrole   
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="row">
            <div class="col-12 d-flex justify-content-center pt-4">
                {{$sites->links()}}
            </div>
        </div>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection