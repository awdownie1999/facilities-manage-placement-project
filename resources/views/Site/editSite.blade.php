@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Edit Site</h1>
        <div>
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
        @endif
        @role('Manager')
            <form method="POST" action="{{ route('sites.update', $site->id) }}">
                @method('PATCH')
                @csrf
                <div>
                    <label for="location">Location</label>
                    <input type="text" name="location" class="form-control" value="{{$site->location}}"/>
                </div><br/>
                <button type="submit" class="btn btn-secondary">Update</button>
            </form>
        @endrole
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection