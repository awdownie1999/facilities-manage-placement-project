@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Dashboard</h1>
        <div>
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
        @endif

        <div class="alert alert-warning fade collapse" role="alert" id="myAlert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                <span class="sr-only">Close</span>
            </button>
        </div>
        <div class="row mb-3">
            <div class="col-xl-3 col-sm-5 py-1">
                <div class="card bg-success text-white h-100">
                    <div class="card-body bg-success">
                        <div class="rotate">
                            <i class="fa fa-user fa-4x"></i>
                        </div>
                            <a href="{{route('bathrooms.index')}}"><h6 class="text-uppercase">Bathrooms</h6>
                            <h1 class="display-4">{{$totalBathrooms}}</h1></a>
                       
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-sm-5 py-2">
                <div class="card text-white bg-danger h-100">
                    <div class="card-body bg-danger">
                        <div class="rotate">
                            <i class="fa fa-list fa-4x"></i>
                        </div>
                            <a href="{{route('organisations.index') }}"><h6 class="text-uppercase">Organisations</h6>
                            <h1 class="display-4">{{ $totalOrg }}</h1></a>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-sm-5 py-2">
                <div class="card-text-third">
                    <div class="card-body bg-info">
                        <div class="rotate">
                            <i class="fa fa-twitter fa-4x"></i>
                        </div>
                        @role('Manager')
                            <a href="{{route('jobs.index')}}"><h6 class="text-uppercase">Jobs In Progress</h6>
                            <h1 class="display-4">{{ $jobsInProgress }}</h1> </a>
                        @else
                            <h6 class="text-uppercase">Jobs In Progress</h6>
                            <h1 class="display-4">{{ $jobsInProgress }}</h1>
                        @endrole
                    </div>
                </div>
            </div>
            
            <div class="col-xl-3 col-sm-5 py-2">
                <div class="card-text-white-bg-new">
                    <div class="card-body">
                        <div class="rotate">
                            <i class="fa fa-share fa-4x"></i>
                        </div>
                        @role('Manager')
                            <a href="{{route('reports.index')}}"><h6 class="text-uppercase">Reports</h6>
                            <h1 class="display-4">{{ $totalReports }}</h1></a>
                        @else
                            <h6 class="text-uppercase">Reports</h6>
                            <h1 class="display-4">{{ $totalReports }}</h1>
                        @endrole
                    </div>
                </div>
            </div>
        </div>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection

<style>

.card-text-white-bg-new{
   background-color:#f78a3b;
   border: none;
}

.card-text-third{
    background-color:#ed07cb;
}

.display-4{
    color:black;
}
.text-uppercase{
    color:black;
}
</style>