@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Account</h1>
        <div>
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
        @endif
        <a class="btn btn-primary" href="{{route('account.edit', Auth::user()->id)}}">Edit Account</a>
        <br/> <br/>
            <table>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>Contact Number</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{Auth::user()->name}}</td>
                        <td>{{Auth::user()->email}}</td>
                        <td>{{Auth::user()->address}}</td>
                        <td>{{Auth::user()->contact_num}}</td>
                    </tr>
                </tbody>
            </table>
            <br/>
            <br/>
            @if(Auth::user()->getMedia('avatars')->first() != null)
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Profile Picture &nbsp;&nbsp;&nbsp;<img src="{{ Auth::user()->getFirstMediaUrl('avatars', 'square') }}">
            <br /><br />
            <form action="{{route('account.destroy', Auth::user()->id)}}" method="POST">
                @csrf
                @method('DELETE')
                &nbsp;&nbsp;&nbsp;<button class="btn btn-primary" type="sumbit"> Delete</button></form>
            @else
                <form method="POST" enctype="multipart/form-data" action="{{ route('account.store', Auth::user()->id) }}">
                    @csrf
                    <input type="file" name="avatar" />
                    <input type="submit" value=" Save " />
                </form>
            @endif

        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection