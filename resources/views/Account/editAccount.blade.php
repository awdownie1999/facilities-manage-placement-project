@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Edit Account</h1>
        <div>
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
        @endif
            <form method="POST" action="{{route('account.update', $user->id)}}">
                @method('PATCH')
                @csrf
                    <div>
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name" value="{{$user->name}}">
                    </div>
                    <div>
                        <label for="email">Email</label>
                        <input type="email" class="form-control" name="email" value="{{$user->email}}">
                    </div>
                    <div>
                        <label for="address">Address</label>
                        <input type="text" class="form-control" name="address" value="{{$user->address}}">
                    </div>
                    <div>
                        <label for="contact_num">Contact Number</label>
                        <input type="text" class="form-control" name="contact_num" value="{{$user->contact_num}}">
                    </div>
                    <button type="submit" class="btn btn-primary">Update Details</button>
            </form>

        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection