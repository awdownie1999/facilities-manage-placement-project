@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Reports</h1>
        <div>
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
        @endif
        @role('Manager')
            <table class="table table-striped"> 
                <thead>
                    <tr>
                        <th>Organisation</th>
                        <th>Bathroom ID</th>
                        <th>Site Location</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <div><a style="margin: 19px;" enctype="multipart/form-data" href ="{{route('reports.create') }}" class="btn btn-primary">New Report</a></div>
                <tbody>
                    @foreach ($reports as $report)
                        <tr>
                            <td>{{$report->bathroom->site->organisation->name}}</td> 
                            <td>{{$report->bathroom->site->location}}</td>
                            <td>{{ $report->bathroom_id }} </td> 
                            <td>{{ $report->description }} </td>
                            <td><a href="{{route('reports.edit', $report->id)}}" class="btn btn-primary">Edit</a></td>
                            <td><form action="{{route('reports.destroy', $report->id)}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" type="submit"> Delete</button></form></td>       
                            @if($report->getMedia('report_images') != null)
                                @foreach ($report->getMedia('report_images') as $image)
                                    <td> &nbsp;&nbsp;&nbsp;<a href="{{ $image->getUrl()}}"> <img src="{{ $image->getUrl('small') }}"> </a>  </td>
                                @endforeach
                            @endif      
                        </tr>
                        @endforeach
                </tbody>
            </table>
        @endrole
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection