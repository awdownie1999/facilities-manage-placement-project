@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Create Report</h1>
        <div>
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
        @endif
        @role('Manager')
           {{--   <select name="site">
                @foreach ($sites as $site )
                    <option value="{{$site->id}}">{{ $site->location}}</option>
                @endforeach
            </select> --}}
            <form method="post" enctype="multipart/form-data" action="{{ route('reports.store') }}">
                @csrf
                <br/><br/>
                <div>
                    <label for="bathroom">Bathroom</label>
                    <select name="bathroom">
                        @foreach ($bathrooms as $bathroom )
                            <option value="{{$bathroom->id}}">{{ $bathroom->id}}</option>
                        @endforeach
                    </select>
                </div><br/>
                <div class="form-group">
                    <label for="description">Description:</label>
                    <input type="text" class="form-control" name="description" />
                </div>
                <br/><br/>
                <input type="file" name="file1" />
                <br/><br/>
                <input type="file" name="file2" />
                <br/><br/>
                <input type="file" name="file3" />
                <br/><br/>
                <button type="submit" class="btn btn-secondary">Add Report</button>
            </form>
        @endrole
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection