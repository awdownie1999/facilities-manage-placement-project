@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Edit Report</h1>
        <div>
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
        @endif
        @role('Manager')
        <form method="post" enctype="multipart/form-data" action="{{ route('reports.update', $report->id)}}">
            @method('PATCH')
            @csrf
            <div class="form-group">
                <label for="description">Description:</label>
                <input type="text" class="form-control" name="description" value="{{ $report->description }}">
            </div>

            <input type="file" name="file1" /> <br/><br/>
            <button type="submit" class="btn btn-secondary">Update</button>
        </form>
        @endrole

        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection