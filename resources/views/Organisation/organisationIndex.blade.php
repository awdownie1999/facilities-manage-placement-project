@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Organisations</h1>
        <div>
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
        @endif
        @role('Manager')
        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Contact Number</th>
                </tr>
            </thead>
            <div><a style="margin:19px;" href="{{route('organisations.create')}}" class="btn btn-primary">New Organisation</a></div>
            <tbody>
                @foreach ($organisations as $organisation)
                    <tr>
                        <td>{{ $organisation->name }}</td>
                        <td>{{ $organisation->contact_number  }}</td>
                        <td><a href="{{route('organisations.edit', $organisation->id)}}" class="btn btn-primary">Edit</a></td>
                        <td><form action="{{route('organisations.destroy', $organisation->id)}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-primary" type="sumbit"> Delete</button></form></td>
                    </tr> 
                @endforeach
            </tbody>
        </table>
    @endrole
    <div class="row">
        <div class="col-12 d-flex justify-content-center pt-4">
            {{$organisations->links()}}
        </div>
    </div>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection