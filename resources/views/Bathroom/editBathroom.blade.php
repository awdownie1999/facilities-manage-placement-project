@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Edit Bathroom Details</h1>
        <div>
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
        @endif
            <form action="{{route('bathrooms.update', $bathroom->id)}}" method="POST">
                @method('PATCH')
                @csrf
                <div>
                    <label for="toilets">Toilets</label>
                    <input type="number" name="toilets" class="form-control" value="{{$bathroom->toilets}}">
                </div>
                <div>
                    <label for="sinks">Sinks</label>
                    <input type="number" name="sinks" class="form-control" value="{{$bathroom->sinks}}">
                </div>
                <input type="file" name="file1" /> <br/><br/>
                <button type="submit" class="btn btn-secondary">Update</button>
            </form>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection