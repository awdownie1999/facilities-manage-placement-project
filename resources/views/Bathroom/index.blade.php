@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Bathrooms</h1>
        <div>
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
        @endif
            <table><thead><tr>
                <th>ID</th>
            </tr></thead><tbody>
                @foreach($bathrooms as $bathroom)
                <tr><td>{{$bathroom->id}}</td>
                    <td><a href="{{route('bathrooms.show', $bathroom->id)}}" class="btn btn-primary">Show Details</a></td>
                    @role('Manager')
                        <td><a href="{{route('bathrooms.edit', $bathroom->id)}}" class="btn btn-primary">Edit</a></td>
                        <td><form action="{{route('bathrooms.destroy', $bathroom->id)}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit"> Delete</button></form></td>
                    @endrole   
            </tr>
            @endforeach</tbody>
        </table>

        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection