@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">New Bathroom</h1>
        <div>
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
        @endif
            <form action="{{route('bathrooms.store')}}" method="POST">
                @csrf
                <label for="toilets">Toilets</label>
                <input type="number" name="toilets" class="form-control"/><br/>
                <label for="sinks">Sinks</label>
                <input type="number" name="sinks" class="form-control"/><br/>
                <label for="site">Site</label>
                <select name="site">
                    @foreach ($sites as $site)
                        <option value="{{$site->id}}">{{$site->location}}</option>
                    @endforeach
                </select><br/><br/>
                <input type="file" name="file1"><br/><br/>
                <input type="file" name="file1"><br/><br/>
                <input type="file" name="file1"><br/><br/>
                <button class="btn btn-secondary" type="submit">New Bathroom</button>
            </form>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection