@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Details</h1>
        <div>
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
        @endif
            <table><thead><tr><th>ID</th>
                <th>&nbsp;&nbsp;Site</th>
                <th>&nbsp;&nbsp;Organisation</th>
                <th>&nbsp;&nbsp;Sinks</th>
                <th>&nbsp;&nbsp;Toilets</th>
                <th>&nbsp;&nbsp;Images</th>
            </tr></thead><tbody><tr><td>{{$bathroom->id}}</td>
                <td>&nbsp;&nbsp;{{$bathroom->site->location}}</td>
                <td>&nbsp;&nbsp;{{$bathroom->site->organisation->name}}</td>
                <td>&nbsp;&nbsp;{{$bathroom->sinks}}</td>
                <td>&nbsp;&nbsp;{{$bathroom->toilets}}</td>

                @if($bathroom->getMedia('bathroom_images') != null)
                    @foreach ($bathroom->getMedia('bathroom_images') as $image)
                        <td> &nbsp;&nbsp;&nbsp;<a href="{{ $image->getUrl()}}"> <img src="{{ $image->getUrl('small') }}"> </a>  </td>
                    @endforeach
                @endif     
                </tr></tbody></table>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection